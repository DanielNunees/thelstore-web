## Web site created to show jobs positions available for L.S Company

## Available Scripts

In the project directory, run:

### `npm install`
### `npm start`
Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

## Tips
### Leave the search field blank to retrieve all the data.

## Dependencies [1]

- Axios (https://www.npmjs.com/package/axios)

### Created by Daniel Nunes