import './App.css';
import style from './layout.module.css';
import Jobs from './components/jobs/jobs';
import Navbar from './components/navbar/navbar';
import Footer from './components/footer/footer';
const App = () => {

   return (
      <>
         <div className={style.container}>
            <Navbar />
            <Jobs />
            <Footer />
         </div>

      </>
   );
}

export default App;
