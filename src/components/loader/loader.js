import React from 'react';
import Loader from "react-loader-spinner";
import styles from './loader.module.css';

//TODO
function MyLoader(props) {
   return (
      <div style={{ margin: 'auto' }} className={props.loading ? '' : styles.hide } >
         <Loader
            type="ThreeDots"
            color="#FF8800"
            height={100}
            width={100}
            visible={props.loading}
         />
      </div>
   )
}

export default MyLoader;