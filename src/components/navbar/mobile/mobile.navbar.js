import React from 'react';
import styles from './mobile-navbar.module.css';


const MobileNavbar = (props) => {
   return (
      <div className={styles.mobileBarBox}>
         <div className={` ${styles.sidenav} ${props.actMobileNav ? styles.fullWidth : styles.zeroWidth} `}>
            <button className={styles.closebtn} onClick={props.toogleBar}>&times;</button>
            <button href="#home">Home</button>
            <button href="#services">Services</button>
            <button href="#guides">Guides</button>
            <button href="#about">About us</button>
            <button href="#jobs" className={styles.active}>Jobs</button>
            <button href="#contact">Contact us</button>

         </div>

         <button className={styles.burguer} onClick={props.toogleBar}>
            <div></div>
            <div></div>
            <div></div>
         </button>
      </div>
   );
}

export default MobileNavbar;