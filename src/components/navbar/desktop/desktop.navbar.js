import React from 'react';
import styles from './desktop-navbar.module.css';

const DesktopNavbar = () => {

   return (
      <div className={styles.desktopBarBox}>
         <nav className={styles.desktopNavbar}>
            <button href="#home">Home</button>
            <button href="#services">Services</button>
            <button href="#guides">Guides</button>
            <button href="#about">About us</button>
            <button href="#jobs" className={styles.active}>Jobs</button>
            <button href="#contact">Contact us</button>
         </nav>
      </div>
   );
}

export default DesktopNavbar;