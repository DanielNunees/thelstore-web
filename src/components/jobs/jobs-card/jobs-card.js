import React from 'react';
import cardStyle from './jobs-card.module.css';

// Card job component
// Show the data of one job at the time
function JobsCard({ job, error }) {
   const formatDate = () => {
      return new Date(job.created).toLocaleDateString();
   }
   return (
      <>
         {error === true ? "Server Error (500)" :
            <div className={`${cardStyle.orange} ${cardStyle.cardSplit} ${cardStyle.card}`}>
               <div>
                  <h2>{job.title}</h2>
                  <p className={cardStyle.salary}>Salary: ${job.salary}</p>
                  <p>Job Type: {job.jobType.type}</p>
                  <p>{job.description}</p>
                  <p>Date Posted: {formatDate()}</p>
               </div>
               <div className={cardStyle.cardFooter}>
                  <p className={cardStyle.applyBtn} ><a href="/">Apply Now <i className="fa fa-arrow-right"></i></a></p>
                  <img src="https://assets.codepen.io/2301174/icon-supervisor.svg" alt="" />
               </div>
            </div>
         }
      </>
   );
}

export default JobsCard;