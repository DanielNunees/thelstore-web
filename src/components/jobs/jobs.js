import React, { useState, useEffect, useMemo } from 'react';
import LoadingContext from '../loader/loading-context';
import JobsGridList from './grid-list/jobs-grid-list';
import jobsService from './jobs-service';

// Main component
// This component will set the initial data in the grid or list view.
function Jobs() {
   const [jobs, setJobs] = useState([]);
   const [loading, setLoading] = useState(false);
   const providerValue = useMemo(() => ({ loading, setLoading }), [loading, setLoading]);

   useEffect(() => {
      // Using the async await function
      const fetchData = async () => {
         setLoading(true);
         const jobs = await jobsService.getJobs('');
         setLoading(false);
         setJobs(jobs);
      }

      // Using promises
      // const fetchData = async () => {
      //    const aux = await JobsService.apiGetPromise("");
      //    setJobs(aux);
      // }
      fetchData();
      // Update the grid using the data from the api
   }, []);

   return (
      <LoadingContext.Provider value={providerValue}>
         <JobsGridList jobs={jobs} />
      </LoadingContext.Provider>

   );
}

export default Jobs;