import React, { useContext, useState } from 'react';
import styles from './jobs-search-form.module.css'
import jobsService from '../jobs-service';
import LoadingContext from '../../loader/loading-context';

//Component responsible for getting the user input for the job search
function JobSearchForm({ searchResultsCallback }) {
   // const [searchResults, setSearchResults] = useState(props);
   const [title, setTitle] = useState("");
   const [typingTimer, setTypingTimer] = useState(0);
   const { setLoading } = useContext(LoadingContext);


   // Set the title state and timeout to execute function
   const handleChange = (evt) => {
      clearTimeout(typingTimer);
      setTitle(evt.target.value);
      const timeoutId = setTimeout(() => { // Timeout set 
         searchJob(evt.target.value);      // to simulate a 
      }, 1000);                            // real connection
      setTypingTimer(timeoutId);

   }
   // Send a request to the server, with query string
   // Trying to find a job that matches with the title
   // If the user send a empty string, will retrieve all the jobs
   const searchJob = async (title) => {
      searchResultsCallback([]); //clean all the previous jobs 
      const searchUrl = `/search?searchString=` + title;
      setLoading(true); // enable loading animation
      const jobs = await jobsService.getJobs(searchUrl);
      searchResultsCallback(jobs);
      setLoading(false);
   }

   return (
      <div className={`${styles.form__group} ${styles.field}`}>
         <input className={styles.form__field} type="input" placeholder="Search Job Title.." name="search2" value={title} onChange={handleChange} />
         <label htmlFor="name" className={styles.form__label}>Search Job Title..</label>
      </div>
   );
}

export default JobSearchForm;