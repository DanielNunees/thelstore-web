import React from 'react';
import styles from './grid-list.module.css';


const GridListButtons = (props) => {
   // Set css grid class
   const gridView = () => {
      props.setViewCallback("grid");
   }
   //Set List class
   const listView = () => {
      props.setViewCallback('list')
   }

   return (
      <div className={styles.showOrHide}>
         <button className={`${styles.btn} ${props.view === "grid" ? styles.btnActive : ""}`}
            onClick={gridView}> Grid <i className="fa fa-th-large" aria-hidden="true"></i>
         </button>
         <button className={`${styles.btn} ${props.view === "list" ? styles.btnActive : ""}`}
            onClick={listView}> List <i className="fa fa-bars" aria-hidden="true"></i>
         </button>
      </div>);
}

export default GridListButtons;