import axios from 'axios';
import config from '../../app.config.json'

const jobsService = {
   getJobs: async (url) => {
      const promise = new Promise((resolve, reject) => {
         setTimeout(async () => {
            try {
               let response = await axios.get(config.SERVER_URL + url);
               resolve(response.data);
            } catch (err) {
               let error = { 'id': 1, 'error': true, message: err };
               resolve(error); // TypeError: failed to fetch
            }
         }, 1500);
      });
      return promise;
   },
   apiGetPromise: (url) => {
      return axios.get(config.SERVER_URL + url).then(res => {
         const jobs = res.data;
         return jobs;
      }).catch(err => {
         let error = { 'id': 1, 'error': true, message: err };
         return error;
      })

   }
}



export default jobsService;