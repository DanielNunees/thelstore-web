import React from 'react';
import styles from './footer.module.css';
import footerLogo from '../../assests/img/footer-logo.json'


const Footer = () => {
   return (
      <footer className={styles.container}>
         <div className={styles.footer}>
            <div className={styles.boxContainer}>
               <div className={styles.column1}>
                  <div className={styles.footerLogo}>
                     <img src={footerLogo['footer-logo']} alt="footer-logo"></img>
                  </div>
                  <div className={styles.footerText}>
                     <div>thelawstore.com.au is a firm of legal, digital and process specialists, assisting our clients to innovate and disrupt.</div>
                     <div>© Copyright 2021 The Law Store</div>
                  </div>
               </div>
               <div className={styles.footerMenu}>
                  <div className={styles.column2}>
                     <button>Home</button>
                     <button>Services</button>
                     <button>Guides</button>
                  </div>
                  <div className={styles.column3}>
                     <button>About us</button>
                     <button>Contact us</button>
                     <button>Jobs</button>
                  </div>
               </div>
            </div>
         </div>
      </footer>
   );


}

export default Footer;